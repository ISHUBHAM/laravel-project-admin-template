<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/homepage','HomeController@getHomepage' );



Route::get('/homepage/index1','HomeController@getDashboard1');
Route::get('/homepage/index2','HomeController@getDashboard2');
Route::get('/homepage/index3','HomeController@getDashboard3');


Route::get('/homepage/widgate','HomeController@widgate');

Route::get('/homepage/layout/topnav','LayoutController@layoutTopnav');

Route::get('/homepage/layout/topnav-sidebar','LayoutController@layoutSidenav');





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
