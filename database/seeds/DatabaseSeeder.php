<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        factory(App\user::class,50)->create();
        factory(App\profile::class,100)->create();
                factory(App\post::class,50)->create();




    }
}
