<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    function thisUser()
    {
        return $this->belongsTo('App\User'); 
    }
}
